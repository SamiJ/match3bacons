#Match3Bacons

## Put your bacons in a row!

A Match 3 game that you can run in your browser. Herd the animals for points and time bonus, extra points for matching pink bacons!

# [Try it live](https://bbcdn.githack.com/SamiJ/match3bacons/raw/HEAD/webapp/match3/index.html) #


## Features:
* Fully playable game
* Scoring with persistent high score
* Easy to add your own new ideas like new animals, bonuses, turn limits etc.
* Implemented with Javascript using bacon.js for event handling

## Get started
You need npm (node package manager) to install the dependencies.

To get started run

> cd webapp

> npm install

and open webapp/match3/index.html

## Thanks
Graphics by [brebren](https://bitbucket.org/brebren/)